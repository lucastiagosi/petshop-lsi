package br.ecommerce.petshop.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.ecommerce.petshop.enums.TipoPagamentoEnum;

/**
 * @author Lucas Tiago
 * @since Release 01 da aplicação. (25/05/2019) Entidade referente ao pedido
 *        feito pelo cliente, do sistema de Petshop. A entidade conta com as
 *        anotações @Entity para que o JPA entenda que se trata de uma entidade,
 *        e também com a anotação @Table para determinar o nome da entidade no
 *        banco de persistência, @Enumerated para informar o Jpa que o atributo
 *        se trata de um Enum.
 */

@Entity
@Table(name = "pedido")
public class Pedido {

	@Column(name = "cod_pedido", length = 11)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int codigoPedido;

	@Column(name = "cod_entrega")
	private int codigoEntrega;

	@ManyToOne
	@JoinColumn(name = "id_cliente")
	private Cliente cliente;

	@Column(name = "valor_total", length = 11)
	private double valorTotal;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_pagamento")
	private TipoPagamentoEnum tipoPagamentoEnum;

	@Column(name = "data_status", length = 11)
	private LocalDate dataStatus;

	public Pedido() {
		super();
	}

	public int getCodigoPedido() {
		return codigoPedido;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public LocalDate getDataStatus() {
		return dataStatus;
	}

	public void setDataStatus(LocalDate dataStatus) {
		this.dataStatus = dataStatus;
	}

	public TipoPagamentoEnum getTipoPagamentoEnum() {
		return tipoPagamentoEnum;
	}

	public void setTipoPagamentoEnum(TipoPagamentoEnum tipoPagamentoEnum) {
		this.tipoPagamentoEnum = tipoPagamentoEnum;
	}

}
