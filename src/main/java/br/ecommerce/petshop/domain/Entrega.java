package br.ecommerce.petshop.domain;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import br.ecommerce.petshop.embeddables.Endereco;

/**
 * @author Lucas Tiago
 * @since Release 01 da aplicação. (25/05/2019) Entidade referente à entrega do
 *        pedido feito pelo cliente, do sistema de Petshop. A entidade conta com
 *        as anotações @Entity para que o JPA entenda que se trata de uma
 *        entidade, e também com a anotação @Table para determinar o nome da
 *        entidade no banco de persistência.
 */

@Entity
@Table(name = "entrega")
public class Entrega {

	@Column(name = "cod_entrega", length = 11)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int codigoEntrega;

	@Column(name = "cod_pedido", length = 11)
	private int codigoPedido;

	@Column(name = "previsto_para")
	private Date previsao;
	
	@DateTimeFormat
	@Column(name = "data_status")
	private LocalDate dataStatus;

	@Embedded
	private Endereco endereco;

	public Entrega() {
		super();
	}

	public int getCodigoEntrega() {
		return codigoEntrega;
	}

	public int getCodigoPedido() {
		return codigoPedido;
	}

	public void setCodigoPedido(int codigoPedido) {
		this.codigoPedido = codigoPedido;
	}

	public Date getPrevisao() {
		return previsao;
	}

	public void setPrevisao(Date previsao) {
		this.previsao = previsao;
	}

	public LocalDate getDataStatus() {
		return dataStatus;
	}

	public void setDataStatus(LocalDate dataStatus) {
		this.dataStatus = dataStatus;
	}

}
