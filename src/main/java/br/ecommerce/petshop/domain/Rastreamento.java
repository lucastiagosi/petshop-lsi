package br.ecommerce.petshop.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import br.ecommerce.petshop.embeddables.Status;

/**
 * @author Lucas Tiago
 * @since Release 01 da aplicação. (25/05/2019) Entidade referente ao
 *        rastramento do pedido feito pelo cliente, do sistema de Petshop. A
 *        entidade conta com as anotações @Entity para que o JPA entenda que se
 *        trata de uma entidade, e também com a anotação @Table para determinar
 *        o nome da entidade no banco de persistência. Também estou utilizando a
 *        anotação @Embedded para persistir os dados contidos no Status de forma
 *        implícita.
 */

@Entity
@Table(name = "tracking")
public class Rastreamento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cod_tracking", length = 11)
	private int codigoRastreamento;
	
	@ManyToOne
	@JoinColumn(name = "cod_pedido")
	private Pedido pedido;

	@Column(name = "descricao_rastreio", length = 50)
	private String descricao_rastreio;
	
	@DateTimeFormat
	@Column(name = "data_status", length = 50)
	private LocalDate dataStatus;

	@Column(name = "geo_lat", length = 14)
	private String latitude;

	@Column(name = "geo_long", length = 14)
	private String longitude;

	@Embedded
	private Status status;

	public Rastreamento() {
		super();
	}

	public int getCodigoRastreamento() {
		return codigoRastreamento;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public String getDescricao_rastreio() {
		return descricao_rastreio;
	}

	public void setDescricao_rastreio(String descricao_rastreio) {
		this.descricao_rastreio = descricao_rastreio;
	}

	public LocalDate getDataStatus() {
		return dataStatus;
	}

	public void setDataStatus(LocalDate dataStatus) {
		this.dataStatus = dataStatus;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
