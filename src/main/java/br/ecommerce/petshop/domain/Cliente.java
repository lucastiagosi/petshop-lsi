package br.ecommerce.petshop.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.ecommerce.petshop.enums.SituacaoEnum;

/**
 * @author Lucas Tiago
 * @since Release 01 da aplicação. (25/05/2019) Entidade referente ao cliente,
 *        do sistema de Petshop. A entidade conta com as anotações @Entity para
 *        que o JPA entenda que se trata de uma entidade, e também com a
 *        anotação @Table para determinar o nome da entidade no banco de
 *        persistência.
 */
@Entity
@Table(name = "clientes")
public class Cliente {
	@Column(name = "id_cliente", length = 11)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCliente;

	@Column(name = "nome", length = 200)
	private String nome;

	@Column(name = "cpf", length = 14)
	private long cpf;

	@Column(name = "email", length = 200)
	private String email;

	@Column(name = "senha", length = 30)
	private String password;

	@Enumerated(EnumType.STRING)
	@Column(name = "situacao", length = 100)
	private SituacaoEnum situacao;

	public Cliente() {
		super();
	}

	public Cliente(String nome, long cpf, String email, String password) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return idCliente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public SituacaoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoEnum situacao) {
		this.situacao = situacao;
	}

}
