package br.ecommerce.petshop.enums;

public enum TipoPagamentoEnum {
	CARTAO_DE_CREDITO("Pagamento no Cartão de Crédito"), 
	CARTAO_DE_DEBITO("Pagamento no Cartão de Débito"), 
	BOLETO_BANCARIO("Pagamento no Boleto Bancário"),
	TRANSFERENCIA("Pagamento via Transferência Direta");
	
	public String tipoPgt;

	TipoPagamentoEnum(String tipo) {
		this.tipoPgt = tipo;
	}

}
