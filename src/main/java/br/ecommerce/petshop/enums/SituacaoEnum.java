package br.ecommerce.petshop.enums;
/**
 * Enum responsável pela situação do Cliente.
 * @author Lucas Tiago
 *
 */
public enum SituacaoEnum {
	A("Ativo"), P("Pendente"), I("Inativo");

	private String descricao;

	private SituacaoEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
