package br.ecommerce.petshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ecommerce.petshop.domain.Pedido;
import br.ecommerce.petshop.service.PedidoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author Lucas Tiago
 * @since Release 01 da aplicação (02/06/2019).
 * 
 * O controller possui todas as requisições HTTP.
 *
 */
@Api(value = "WebService do Pedido.")
@RestController
public class PedidoController {

	@Autowired
	private PedidoService pedidoService;

	/**
	 * Faz uma requisição @GET para listar todos os pedidos existentes.
	 * @return uma nova lista com todos os pedidos persistidos na aplicação, @OK caso sucesso.
	 */
	@ApiOperation(value = "Lista todos os Pedidos existentes")
	@RequestMapping(value = "/pedido", method = RequestMethod.GET)
	public ResponseEntity<List<Pedido>> listarPedidos() {

		List<Pedido> pedidos = pedidoService.listarPedidos();

		return new ResponseEntity<List<Pedido>>(pedidos, HttpStatus.OK);
	}
	
	/**
	 * Faz uma requisição @GET para exibir um pedido através do seu ID.
	 * @OK caso sucesso, @NOT_FOUND caso não exista.
	 */
	@ApiOperation(value = "Exibe um Pedido por ID")
	@RequestMapping(value = "/pedido/{id}", method = RequestMethod.GET)
	public ResponseEntity<Pedido> exibirPedido(@PathVariable int id) {

		Pedido pedido = pedidoService.buscarPedidoPorId(id);

		if (pedido == null) {
			return new ResponseEntity<Pedido>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Pedido>(pedido, HttpStatus.OK);
		}
	}
	
	/**
	 * Faz uma requisição @POST para inserir um novo pedido.
	 * @return um novo pedido persistindo-o na aplicação, @CREATED caso tenha sucesso e @INTERNAL_SERVER_ERROR caso ocorra algum problema.
	 */
	@ApiOperation(value = "Insere um novo Pedido")
	@RequestMapping(value = "/pedido", method = RequestMethod.POST)
	public ResponseEntity<Pedido> inserirPedido(@RequestBody Pedido pedido) {

		try {
			Pedido pedidoInserido = pedidoService.inserirPedido(pedido);
			return new ResponseEntity<Pedido>(pedidoInserido, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Pedido>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	/**
	 * Faz uma requisição de Update de um Pedido. 
	 */
	@ApiOperation(value = "Atualiza um Pedido Existente")
	@PutMapping(value = "/pedido/{id}")
	public ResponseEntity<Pedido> updatePedido(@PathVariable int id, @RequestBody Pedido pedido){
		pedidoService.atualizarPedido(pedido, id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * Exclui um pedido existente
	 */
	@ApiOperation(value = "Deleta um Pedido Existente")
	@DeleteMapping(value = "/pedido/{id}")
	public ResponseEntity<Pedido> excluirPedido(@PathVariable int id){
		if(id != 0) {			
			pedidoService.excluirPedidoPorId(id);
		} else {			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	public PedidoService getPedidoService() {
		return pedidoService;
	}

	public void setPedidoService(PedidoService pedidoService) {
		this.pedidoService = pedidoService;
	}

}
