package br.ecommerce.petshop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.ecommerce.petshop.domain.Rastreamento;
import br.ecommerce.petshop.dtos.InserirRastreamentoDTO;
import br.ecommerce.petshop.service.RastreamentoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author Lucas Tiago
 * @since Release 01 da aplicação (02/06/2019).
 * 
 * O controller possui todas as requisições HTTP.
 *
 */
@Api(value = "WebService do Rastreamento")
@RestController
public class RastreamentoController {

	@Autowired
	private RastreamentoService rastreamentoService;

	/**
	 * Faz uma requisição @GET para listar todos os rastreamentos existentes.
	 * @return uma nova lista com todos os rastreamentos persistidos na aplicação, @OK caso sucesso.
	 */
	@ApiOperation(value = "Lista os rastreamentos cadastrados")
	@RequestMapping(value = "/rastreamento", method = RequestMethod.GET)
	public ResponseEntity<List<Rastreamento>> listarRastreamentos() {

		List<Rastreamento> rastreamentos = rastreamentoService.listarRastreamentos();

		return new ResponseEntity<List<Rastreamento>>(rastreamentos, HttpStatus.OK);
	}

	/**
	 * Faz uma requisição @GET para exibir um rastreamento existente.
	 * @return um novo rastreamento persistido na aplicação, @OK caso obtenha sucesso, @NOT_FOUND caso não exista.
	 */
	@ApiOperation(value = "Retorna um rastreamento através do id")
	@RequestMapping(value = "/rastreamento/{id}", method = RequestMethod.GET)
	public ResponseEntity<Rastreamento> exibirRastreamento(@PathVariable int id) {

		Rastreamento rastreamento = rastreamentoService.buscarRastreamentoPorId(id);

		if (rastreamento == null) {
			return new ResponseEntity<Rastreamento>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Rastreamento>(rastreamento, HttpStatus.OK);
		}
	}
	/**
	 * Faz uma requisição @POST para inserir um novo rastreamento.
	 * @return um novo rastreamento persistindo-o na aplicação, @CREATED caso tenha sucesso e @INTERNAL_SERVER_ERROR caso ocorra algum problema.
	 */
	@ApiOperation(value = "Insere um novo Rastreamento")
	@RequestMapping(value = "/rastreamento", method = RequestMethod.POST)
	public ResponseEntity<Rastreamento> inserirRastreamento(@RequestBody InserirRastreamentoDTO rastreio) {

		try {
			Rastreamento rastreamentoInserido = rastreamentoService.inserirRastreamento(rastreio);
			return new ResponseEntity<Rastreamento>(rastreamentoInserido, HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Rastreamento>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	/**
	 * Faz uma requisição para atualizar um Rastreamento Existente.
	 */
	@ApiOperation(value = "Atualiza um Rastreamento Existente.")
	@PutMapping(value = "/rastreamento/{id}")
	public ResponseEntity<Rastreamento> updateRastreamento(@PathVariable int id, @RequestBody Rastreamento rastreio){
		rastreamentoService.atualizarRastreamento(rastreio, id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	/**
	 * Faz uma requisição para excluir um Rastreamento Existente.
	 */
	@ApiOperation(value = "Deleta um Rastreamento Existente")
	@DeleteMapping(value = "rastreamento/{id}")
	public ResponseEntity<Rastreamento> excluirRastreamento(@PathVariable int id){
		if(id != 0) {			
			rastreamentoService.excluirRastreamentoPorId(id);
		} else {			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	public RastreamentoService getRastreamentoService() {
		return rastreamentoService;
	}

	public void setRastreamentoService(RastreamentoService rastreamentoService) {
		this.rastreamentoService = rastreamentoService;
	}

}
