package br.ecommerce.petshop.dtos;

import java.time.LocalDate;

import br.ecommerce.petshop.embeddables.Status;

/**
 * DTO responsável por inserir um novo Rastreamento, convertendo-o para objeto.
 * 
 * @author Lucas Tiago
 *
 */
public class InserirRastreamentoDTO {
	private int codigoPedido;
	private LocalDate dataStatus;
	private String descricao;
	private String latitude;
	private String longitude;
	private Status status;

	public int getCodigoPedido() {
		return codigoPedido;
	}

	public void setCodigoPedido(int codigoPedido) {
		this.codigoPedido = codigoPedido;
	}

	public LocalDate getDataStatus() {
		return dataStatus;
	}

	public void setDataStatus(LocalDate dataStatus) {
		this.dataStatus = dataStatus;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
