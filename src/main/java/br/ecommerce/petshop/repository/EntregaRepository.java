package br.ecommerce.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.ecommerce.petshop.domain.Entrega;

/**
 * 
 * @author Lucas Tiago
 *  Em tempo de execução, o Spring Data REST criará uma implementação dessa interface automaticamente. 
 *  Em seguida, ele usará a anotação @RepositoryRestResource para direcionar 
 *  o Spring MVC para criar endpoints RESTful em /entrega.
 *
 */
@RepositoryRestResource(collectionResourceRel = "entrega", path = "entrega")
public interface EntregaRepository extends JpaRepository<Entrega, Integer> {

}
