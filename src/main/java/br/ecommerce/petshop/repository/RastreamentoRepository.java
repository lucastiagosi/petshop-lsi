package br.ecommerce.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ecommerce.petshop.domain.Rastreamento;


@Repository
public interface RastreamentoRepository extends JpaRepository<Rastreamento, Integer> {


}
