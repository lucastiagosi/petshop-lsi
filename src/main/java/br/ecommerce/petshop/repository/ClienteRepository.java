package br.ecommerce.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ecommerce.petshop.domain.Cliente;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {


}
