package br.ecommerce.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.ecommerce.petshop.domain.Pedido;


@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Integer> {


}
