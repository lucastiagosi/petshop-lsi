package br.ecommerce.petshop.embeddables;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

/**
 * @author Lucas Tiago
 * @since Release 01 da aplicação. (25/05/2019) Entidade referente ao Status do
 *        rastreamento do cliente.
 */
@Embeddable
public class Status {

	@GeneratedValue(strategy = GenerationType.AUTO)
	private String codigoStatus;

	@Column(name = "descricao", length = 40)
	private String descricao;

	public Status() {
		super();
	}

	public String getCodigoStatus() {
		return codigoStatus;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
