package br.ecommerce.petshop.embeddables;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/** 
 * @author Lucas Tiago
 * @since Release 01 da aplicação. (25/05/2019)
 * Entidade referente ao Endereco de entrega de Entrega.
 */
@Embeddable
public class Endereco {

	@Column(name = "rua", length = 100)
	private String rua;

	@Column(name = "bairro", length = 100)
	private String bairro;

	@Column(name = "numero", length = 200)
	private int numero;

	@Column(name = "complemento", length = 50)
	private String complemento;

	@Column(name = "cidade", length = 50)
	private String cidade;

	@Column(name = "uf", length = 2)
	private String uf;

	public Endereco() {
		super();
	}

	public Endereco(String rua, String bairro, int numero, String complemento, String cidade, String uf) {
		super();
		this.rua = rua;
		this.bairro = bairro;
		this.numero = numero;
		this.complemento = complemento;
		this.cidade = cidade;
		this.uf = uf;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

}
