package br.ecommerce.petshop.config;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	private static String title = "Petshop da Tayane";
	private static String description = "E-Commerce com produtos voltados para animais!";
	private static String version = "1.0.0";
	private static String termsOfServiceUrl = "Terms of Service";
	private Contact contact = new Contact("Lucas Tiago C Silva", null, "lucastiagocav@gmail.com");
	private static String license = "Apache Version 2.0";
	private static String licenseUrl = "http://apache.org/license.html";
	/*
	 * Collection<VendorExtension> vendorExtensions;
	 */

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("br.ecommerce.petshop")).build().apiInfo(metaInfo());
	}

	private ApiInfo metaInfo() {
		ApiInfo apiInfo = new ApiInfo(title, description, version, termsOfServiceUrl, contact, license, licenseUrl,
				new ArrayList<VendorExtension>());
		return apiInfo;
	}
}
