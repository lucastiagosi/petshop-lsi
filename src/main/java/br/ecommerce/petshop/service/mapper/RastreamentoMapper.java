package br.ecommerce.petshop.service.mapper;
/**
 * O mapper é responsável pela conversão do Objeto DTO para o Objeto obtido no Service.
 * @author Lucas Tiago
 *
 */

import br.ecommerce.petshop.domain.Rastreamento;
import br.ecommerce.petshop.dtos.InserirRastreamentoDTO;

public class RastreamentoMapper {

	public static Rastreamento mapper(InserirRastreamentoDTO rastreamento) {
		Rastreamento rastreamentoConvertido = new Rastreamento();
		rastreamentoConvertido.setDataStatus(rastreamento.getDataStatus());
		rastreamentoConvertido.setDescricao_rastreio(rastreamento.getDescricao());
		rastreamentoConvertido.setLatitude(rastreamento.getLatitude());
		rastreamentoConvertido.setLongitude(rastreamento.getLongitude());
		rastreamentoConvertido.setStatus(rastreamento.getStatus());
		return rastreamentoConvertido;
	}
}
