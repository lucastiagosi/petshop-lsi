package br.ecommerce.petshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import br.ecommerce.petshop.domain.Pedido;
import br.ecommerce.petshop.enums.TipoPagamentoEnum;
import br.ecommerce.petshop.repository.PedidoRepository;

/**
 * Service responsável por implementar as regras de negócio do Pedido.
 * 
 * @author Lucas Tiago
 *
 */
@Service
@Validated
public class PedidoService {
	private static final double DESCONTO = 0.10;

	@Autowired
	private PedidoRepository repository;

	@Autowired
	private ClienteService clienteService;

	/**
	 * Regra de negócio: Caso a compra seja à vista, o comprador ganha 10% de
	 * desconto.
	 */

	@Transactional
	public Pedido inserirPedido(Pedido pedido) {
		double valorPedido;
		clienteService.inserirCliente(pedido.getCliente());
		pedido.setCliente(clienteService.buscarClientePorId(pedido.getCliente().getId()));
		if (pedido.getTipoPagamentoEnum().equals(TipoPagamentoEnum.BOLETO_BANCARIO)) {
			valorPedido = pedido.getValorTotal() - (pedido.getValorTotal() * DESCONTO);
			pedido.setValorTotal(valorPedido);
		}
		return repository.save(pedido);
	}
	/**
	 *  Através do Id, atualiza o Pedido existente.
	 * @param pedido
	 * @param id
	 */

	@Transactional
	public Pedido atualizarPedido(Pedido pedido, int id) {
		Pedido pedidoTemp = buscarPedidoPorId(id);
		clienteService.inserirCliente(pedido.getCliente());
		pedidoTemp.setDataStatus(pedido.getDataStatus());
		pedidoTemp.setCliente(clienteService.buscarClientePorId(pedido.getCliente().getId()));
		return repository.save(pedidoTemp);
	}
	/**
	 * Busca o pedido através do parametro Id.
	 * @param id
	 * @return
	 */
	public Pedido buscarPedidoPorId(int id) {
		return repository.findById(id).get();
	}
	/**
	 * Retorna uma lista com todos os pedidos existentes.
	 * @return
	 */
	public List<Pedido> listarPedidos() {
		return repository.findAll();
	}
	/**
	 * Exclui um pedido existente.
	 * @param id
	 */
	@Transactional
	public void excluirPedidoPorId(int id) {
		repository.deleteById(id);
	}
}
