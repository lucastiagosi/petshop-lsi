package br.ecommerce.petshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import br.ecommerce.petshop.domain.Pedido;
import br.ecommerce.petshop.domain.Rastreamento;
import br.ecommerce.petshop.dtos.InserirRastreamentoDTO;
import br.ecommerce.petshop.repository.RastreamentoRepository;
import br.ecommerce.petshop.service.mapper.RastreamentoMapper;

/**
 * Service responsável pelas regras de negócio do Rastreamento.
 * 
 * @author Lucas Tiago
 *
 */
@Service
@Validated
public class RastreamentoService {

	@Autowired
	private RastreamentoRepository repository;

	@Autowired
	private PedidoService pedidoService;


	/**
	 * Método responsável pela inserção dos Rastreamentos
	 * 
	 * @param rastreio
	 * @return
	 */
	@Transactional
	public Rastreamento inserirRastreamento(InserirRastreamentoDTO rastreio) {
		
		Pedido pedido = pedidoService.buscarPedidoPorId(rastreio.getCodigoPedido());
		Rastreamento rastreamento = RastreamentoMapper.mapper(rastreio);
		if (pedido != null) {
			rastreamento.setStatus(rastreio.getStatus());
			rastreamento.setPedido(pedido);
			rastreamento.setDescricao_rastreio("Pedido efetuado com sucesso!");
			rastreamento.setStatus(rastreio.getStatus());
		}
		return repository.save(rastreamento);
	}

	/**
	 * Método responsável pela atualização do Rastreamento.
	 * 
	 * @param rastreio
	 * @param id
	 */
	@Transactional
	public Rastreamento atualizarRastreamento(Rastreamento rastreio, int id) {

		Rastreamento rastreamento = buscarRastreamentoPorId(id);
		rastreamento.setDataStatus(rastreio.getDataStatus());
		rastreamento.setDescricao_rastreio(rastreio.getDescricao_rastreio());
		rastreamento.setLatitude(rastreio.getLatitude());
		rastreamento.setLongitude(rastreio.getLongitude());
		rastreamento.setPedido(rastreio.getPedido());
		rastreamento.setStatus(rastreio.getStatus());

		return repository.save(rastreamento);
	}

	/**
	 * Método responsável pela obtenção de um rastreamento, através do id.
	 * 
	 * @param id
	 * @return
	 */
	public Rastreamento buscarRastreamentoPorId(int id) {
		return repository.findById(id).get();
	}

	/**
	 * Método responsável por listar todos os Rastreamentos da aplicação.
	 * 
	 * @return
	 */
	public List<Rastreamento> listarRastreamentos() {
		return repository.findAll();
	}

	/**
	 * Método responsável pela exclusão de um rastreamento.
	 * 
	 * @param id
	 */
	@Transactional
	public void excluirRastreamentoPorId(int id) {
		repository.deleteById(id);
	}

}
