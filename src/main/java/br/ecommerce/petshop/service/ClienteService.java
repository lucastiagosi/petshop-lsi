package br.ecommerce.petshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import br.ecommerce.petshop.domain.Cliente;
import br.ecommerce.petshop.repository.ClienteRepository;
/**
 * Service responsável pelas regras de negócio do Cliente.
 * @author Lucas Tiago
 *
 */
@Service
@Validated
public class ClienteService {

	@Autowired
	private ClienteRepository repository;
/**
 * Método para inserção do Cliente na aplicação.
 * @param cliente
 * @return
 */
	@Transactional
	public Cliente inserirCliente(Cliente cliente) {
		return repository.save(cliente);
	}
/**
 * Método para atualização do Cliente na aplicação.
 * @param cliente
 * @param id
 */
	@Transactional
	public void atualizarCliente(Cliente cliente, int id) {
		Cliente c = buscarClientePorId(id);
		c.setCpf(cliente.getCpf());
		c.setEmail(cliente.getEmail());
		c.setNome(cliente.getNome());
		c.setPassword(cliente.getPassword());
		repository.save(c);
	}
/**
 * Método responsável por buscar um cliente por um id.
 * @param id
 * @return
 */
	public Cliente buscarClientePorId(int id) {
		return repository.findById(id).get();
	}
/**
 * Método responsável pela busca de todos os clientes da aplicação.
 * @return
 */
	public List<Cliente> listarClientes() {
		return repository.findAll();
	}
/**
 * Método responsável pela exclusão de um determinado cliente.
 * @param id
 */
	@Transactional
	public void excluirClientePorId(int id) {
		repository.deleteById(id);
	}
}
